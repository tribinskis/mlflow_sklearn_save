import mlflow
from lightgbm import LGBMRegressor
from sklearn.datasets import load_digits
from sklearn.pipeline import make_pipeline

from src.pipeline.pipe import pca


def main():
    X, y = load_digits(return_X_y=True)
    model = LGBMRegressor(
        boosting_type="dart",
        objective="regression",
    )
    model = make_pipeline(pca, model)
    model.fit(X, y)
    mlflow.sklearn.save_model(sk_model=model, path="LGBMRegressorModel", code_paths=["src/pipeline"])


if __name__ == "__main__":
    main()
